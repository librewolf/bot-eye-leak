module.exports = {
	apps: [
		{
			name: require('package.json').name,
			script: require('package.json').main,
			max_memory_restart: '1000M',
			wait_ready: true,
			restart_delay: 5000,
			env_production: {
				NODE_ENV: 'production'
			}
		}
	]
}
