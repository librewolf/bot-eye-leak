const { Schema, model } = require('mongoose')

const eyeDatabase = new Schema({
	id: String,
	phone: String,
	username: String,
	first_name: String,
	last_name: String
})

module.exports = model('Eyeofgod', eyeDatabase)
