const { Schema, model } = require('mongoose')

const modelUser = new Schema({
	user_id: {
		type: Number,
		unique: true,
		index: true,
		required: true
	},

	first_name: {
		type: String,
		required: true
	},

	username: String,

	settings: {
		lang: {
			type: String,
			default: 'en',
			required: true
		}
	}
})

module.exports = model('User', modelUser)
