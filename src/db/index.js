const mongoose = require('mongoose')
const { cS } = require('../config/config')

class Mongoose {
	async mongooseConnect() {
		await mongoose
			.connect(process.env.MONGOOSE_URL, {
				keepAlive: true,
				useCreateIndex: true,
				useNewUrlParser: true,
				useUnifiedTopology: true,
				socketTimeoutMS: 10000,
				connectTimeoutMS: 30000
			})

			.catch(err => {
				console.error(err.stack)
				console.error(
					cS.red.bold(`Mongoose error: ${process.env.MONGOOSE_URL}`)
				)
				process.exit(0)
			})

		process.on('SIGINT', () => {
			mongoose.connection.close(() => {
				console.log(`Disconnect ${process.env.MONGOOSE_URL}`)
				process.exit(0)
			})
		})
	}
}

module.exports = new Mongoose()
