const { SERVICE } = require('../util/service')

class EyeofgodTask {
	checkLeak = async ({ match, db, i18n, Extra, message, replyWithHTML }) => {
		try {
			const id = match[0]
			const base = await db.Eye.findOne({ id }).sort({ id: 1 }).lean()
			// const expectData = await SERVICE.getInfoOperator(base?.phone)
			return await replyWithHTML(
				i18n.t('eyeofgod', {
					id: base.id,
					nPhone: base?.phone,
					username: base?.username,
					fullName: `${base?.first_name ?? ''} | ${base?.last_name ?? ''}`
					// ...expectData
				}),
				Extra.HTML()
					.inReplyTo(message.message_id)
					.webPreview(false)
					.markup(m =>
						m.inlineKeyboard(
							[
								m.urlButton('❤️ Subscribe', 'https://t.me/ThisOpenSource'),
								m.callbackButton(i18n.t('default.closeMessage'), 'delete')
							],
							{ columns: 2 }
						)
					)
			)
		} catch {
			return await replyWithHTML(
				'📍 <b>There is no database !</b>',
				Extra.HTML()
					.inReplyTo(message.message_id)
					.webPreview(false)
					.markup(m =>
						m.inlineKeyboard(
							[m.callbackButton(i18n.t('default.closeMessage'), 'delete')],
							{ columns: 2 }
						)
					)
			)
		}
	}
}

module.exports = new EyeofgodTask()
