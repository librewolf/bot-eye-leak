const { readdirSync, path } = require('../config/config')

class LangTask {
	updaterLang = async ({ i18n, Extra, Markup, replyWithHTML }) => {
		const locales = {}
		const markupArr = []
		const __PATH__ = path.resolve(__dirname, '../locales')

		readdirSync(__PATH__).forEach(fileName => {
			const value = fileName.split('.')[0]
			locales[value.toString()] = { name: i18n.t(value) }
		})

		Object.keys(locales).forEach(value => {
			let name = locales[value.toString()].name
			if (value === i18n.languageCode) name = `✅ ${name}`
			return markupArr.push(Markup.callbackButton(name, `set_lang|${value}`))
		})

		return await replyWithHTML(
			i18n.t('default.lang'),
			Extra.HTML().markup(m => m.inlineKeyboard(markupArr))
		)
	}
}

module.exports = new LangTask()
