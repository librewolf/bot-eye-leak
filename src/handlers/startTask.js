class StartTask {
	async start({ reply, replyWithSticker, message, i18n, from, Extra }) {
		await replyWithSticker(
			'CAACAgIAAxkBAAIZH2DvF-OAI9Kzy_QazXqHYR0rUD7-AAIXAAPDVgMeQjlqcLJ92EYgBA'
		)
		return await reply(
			i18n.t('main.start', { ...from }),
			Extra.HTML()
				.inReplyTo(message.message_id)
				.webPreview(false)
				.markup(m =>
					m.inlineKeyboard(
						[
							m.callbackButton('💰 Donate', 'startMenu|donate'),
							m.urlButton('❤️ My Notabug', 'https://notabug.org/Secven'),
							m.urlButton('😊 Subscribe', 'https://t.me/ThisOpenSource'),
							m.callbackButton(i18n.t('default.closeMessage'), 'delete')
						],
						{ columns: 2 }
					)
				)
		)
	}

	donateMe = async ({ i18n, Extra, editMessageText }) => {
		return editMessageText(
			i18n.t('main.donate'),
			Extra.HTML().markup(m =>
				m.inlineKeyboard([
					m.callbackButton(i18n.t('default.closeMessage'), 'delete')
				])
			)
		)
	}
}

module.exports = new StartTask()
