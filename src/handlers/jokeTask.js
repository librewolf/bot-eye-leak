const { SERVICE } = require('../util/service')

class JokeTask {
	async getRandomAnimation({ Extra, replyWithAnimation }) {
		const { videoURL: sendGif, description: caption } =
			await SERVICE.getJokeDev()
		return replyWithAnimation(sendGif, {
			caption,
			...Extra.markup(m =>
				m.inlineKeyboard([m.callbackButton('MOAR', 'joke|moar')])
			)
		})
	}

	async updateAnimation({
		Extra,
		answerCbQuery,
		editMessageMedia,
		editMessageCaption
	}) {
		await answerCbQuery()
		const moar = Extra.markup(m =>
			m.inlineKeyboard([m.callbackButton('MOAR', 'joke|moar')])
		)
		const { videoURL: media, description: caption } = await SERVICE.getJokeDev()
		const objMedia = { type: 'animation', media }
		return await editMessageMedia(objMedia, moar).then(() =>
			editMessageCaption(caption, moar)
		)
	}
}

module.exports = new JokeTask()
