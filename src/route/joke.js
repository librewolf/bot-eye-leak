module.exports = bot => {
	const { jokeTask } = bot.handlers

	bot.hears(['/joke', '!joke', '!jo'], jokeTask.getRandomAnimation)

	const routeList = ctx => {
		switch (ctx.state.value) {
			case 'moar':
				return jokeTask.updateAnimation(ctx)
		}
	}

	bot.route.on('joke', routeList)
}
