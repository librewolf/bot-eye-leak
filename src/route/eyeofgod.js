module.exports = bot => {
	const { eyeofgodTask } = bot.handlers

	const expect = bot.rateLimit({
		window: 20e3,
		limit: 1,
		onLimitExceeded: ({ replyWithHTML }) =>
			replyWithHTML(
				'<code>I can handle from you only one request in 15 seconds.</code>',
				true,
				{ disable_notification: true }
			)
	})

	bot.hears(/^[0-9]{5,20}$/g, expect, eyeofgodTask.checkLeak)

	// TODO: Select menu add
}
