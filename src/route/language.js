module.exports = bot => {
	const { langTask } = bot.handlers

	bot.command('lang', langTask.updaterLang)

	const routeList = async ({
		i18n,
		state,
		session,
		answerCbQuery,
		editMessageText
	}) => {
		if (state.value) {
			await answerCbQuery(state.value).then(() => {
				session.root.settings.lang = state.value
				i18n.locale(state.value)
				return editMessageText(i18n.t('default.langDone', { ...state }))
			})
		}
	}

	bot.route.on('set_lang', routeList)
}
