const fetch = require('node-fetch')

const getJokeDev = async () => {
	return Promise.resolve(
		await fetch(
			`https://developerslife.ru/top/${+~~(Math.random() * 10e1)}?json=true`
		)
	)
		.then(res => res.json())
		.then(data => {
			const index = +~~(Math.random() * data.result.length)
			return data.result[+index]
		})
		.catch(err => console.error(err.message()))
}

const getInfoOperator = async (num = '79521305638') => {
	// eslint-disable-next-line no-async-promise-executor
	return new Promise(async resolve => {
		const url = 'https://cleaner.dadata.ru/api/v1/clean/phone'
		await fetch(url, {
			method: 'POST',
			mode: 'cors',
			headers: {
				Authorization: 'Token ' + process.env.DADATA_TOKEN,
				'X-Secret': process.env.DADATA_SECRET,
				'Content-Type': 'application/json'
			},
			compress: true,
			body: JSON.stringify([+num])
		})
			.then(res => {
				if (res.statusCode === 401)
					throw new Error('dadata.ru:: key or secret invalid')

				if (res.statusCode === 403)
					throw new Error('dadata.ru:: invalid secret key')
				return res.json()
			})
			.then(data => resolve(...data))
			.catch(err => console.error('end dadata.ru::', err.message))
	})
}

const SERVICE = {
	getInfoOperator: getInfoOperator,
	getJokeDev: getJokeDev
}

module.exports = { SERVICE }
