const { readdirSync } = require('fs')
const path = require('path')
const oS = require('os')
const cS = require('chalk')

const config = {}

config.isDev = process.env.NODE_ENV === 'development'

config.consignOpts = {
	cwd: process.cwd() + '/src',
	extensions: ['.js'],
	verbose: false
}

config.limitSecurity = {
	window: 20e3,
	limit: 8,
	onLimitExceeded: ({ reply }) =>
		reply('Please wait a second', {
			disable_notification: true
		})
}

config.cbQueryOpts = {
	window: 15e3,
	limit: 3,
	onLimitExceeded: ({ answerCbQuery }) =>
		answerCbQuery('Please wait a second', true)
}

config.i18nOpts = {
	directory: path.resolve(__dirname, '../locales'),
	defaultLanguage: 'en'
}

config.settings = {
	setCommand: async (ctx, next) => {
		ctx.setMyCommands([
			{
				command: 'start',
				description: 'Start bot or restart'
			},
			{
				command: 'lang',
				description: 'Change language'
			},
			{
				command: 'joke',
				description: 'IT GIfs or !joke'
			}
		])
		return next()
	}
}

module.exports = {
	config,
	readdirSync,
	path,
	oS,
	cS
}
