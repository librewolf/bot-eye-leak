const {
	Telegraf,
	Router,
	Markup,
	Extra,
	Composer,
	session
} = require('telegraf')
const { I18n } = require('telegraf-i18n')
const { db } = require('../db/db')
const { config } = require('./config')
const { rateLimit, reportErr, updaterUser } = require('../middle')
const {
	generateUpdateMiddleware: cliUpdateTime
} = require('telegraf-middleware-console-time')
const consign = require('consign')

const bot = new Telegraf(process.env.BOT_TOKEN, {
	telegram: {
		apiRoot: process.env.TG_APIROOT
	},
	handlerTimeout: 1
})

const Route = new Router(({ callbackQuery }) => {
	if (!callbackQuery.data) return
	const [route, value] = callbackQuery.data.split('|')
	return {
		route,
		state: { value }
	}
})

;(async () => {
	if (config.isDev) {
		// Dev handlers
		bot.use(cliUpdateTime())
		bot.use(Telegraf.log())
		await bot.use(config.settings.setCommand)
		const { TG_APIROOT, BOT_CLUSTER_CORE } = process.env
		console.log(
			Object.assign(await bot.telegram.getMe(), {
				BOT_CLUSTER_CORE,
				TG_APIROOT
			})
		)
	}
})()

bot.route = Route
bot.rateLimit = rateLimit

bot.context = {
	db,
	Extra,
	Markup,
	Composer
}

bot.use(rateLimit(config.limitSecurity))
bot.use(Composer.mount('callback_query', rateLimit(config.cbQueryOpts)))
bot.use(session())
bot.use(new I18n(config.i18nOpts))
bot.use(updaterUser)
bot.use(reportErr)

bot.on('callback_query', Route)
bot.route.otherwise(({ deleteMessage }) => deleteMessage())

consign(config.consignOpts).include('handlers').then('route').into(bot)

bot.use(({ reply, i18n, message }) =>
	reply(
		i18n.t('default.noAnswer'),
		Extra.HTML()
			.inReplyTo(message.message_id)
			.markup(m =>
				m.inlineKeyboard([
					m.callbackButton(i18n.t('default.closeMessage'), 'delete')
				])
			)
	)
)

bot.catch(err => console.error(err.message))

module.exports = { bot }
