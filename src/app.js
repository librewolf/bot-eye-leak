const cluster = require('cluster')
const { mongooseConnect } = require('./db')
const { config, oS, cS } = require('./config/config')
const { bot: initTelegraf } = require('./config/telegraf')

const __CORE__ = Number(process.env.BOT_CLUSTER_CORE)

class App {
	constructor(setWorker = 0) {
		this.isCore = oS.cpus().length
		this.init(setWorker).then(() =>
			console.log(cS.green.bold(`Mongoose connected ${process.pid}`))
		)
	}

	init = async core => {
		await mongooseConnect().then(() => {
			return config.isDev
				? initTelegraf
						.launch()
						.then(() => console.log(cS.blue.bold('Mode: Development')))
						.catch(err => err.message)
				: this.createCluster(core)
		})
	}

	createCluster = async setWorker => {
		const _workers = []

		if (setWorker === 0) setWorker = this.isCore

		if (setWorker > this.isCore)
			throw new Error(`Cluster error: Max core only [ ${this.isCore} ]`)

		if (cluster.isMaster) {
			console.log(
				`****** Creating instances [ ${cS.green.bold(setWorker)} ] ******`
			)
			for (let i = 0; i < setWorker; i += 1) {
				cluster.schedulingPolicy = cluster.SCHED_NONE
				const worker = cluster.fork()
				_workers.push(worker)
			}

			cluster.on('exit', worker => {
				cluster.fork()
				console.log(cS.red.bold(`Worker ${worker.process.pid} died`))
			})

			console.log(cS.green.bold('****** Mode: Started to Production ******'))
		} else {
			console.info(cS.blue(`[ Worker to ${process.pid} started ]`))
			process.on('message', update => {
				initTelegraf.handleUpdate(update)
			})
		}

		if (cluster.isMaster) {
			initTelegraf.use(ctx => handlerUpdater(ctx))
			const starter = initTelegraf.launch()
			this.killGetUpdates(starter).catch(err => console.error(err.message))
		}

		let clusterNumber = 0

		const handlerUpdater = ctx => {
			if (clusterNumber >= _workers.length) clusterNumber = 0
			const worker = _workers[+clusterNumber]
			if (worker) {
				clusterNumber += 1
				worker.send(ctx.update)
			}
		}
	}

	killGetUpdates = async callback => {
		await initTelegraf.telegram
			.callApi('getUpdates', { offset: -1 })
			.then(updates => updates.length && updates[0].update_id + 1)
			.then(offset => {
				if (offset)
					return initTelegraf.telegram.callApi('getUpdates', {
						offset
					})
			})
			.then(() => callback)
			.catch(err => console.error(err.message))
	}
}

new App(__CORE__)
