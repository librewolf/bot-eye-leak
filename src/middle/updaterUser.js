const { escapeHtml } = require('../util/escapeHTML')

module.exports = async ({ db, from, i18n, session }, next) => {
	const { id, first_name: firstName, username } = from
	if (!id) return await next()
	await db.User.findOne({ user_id: id })
		.then(root => {
			if (!root) {
				root = new db.User()
				root.user_id = id
				root.first_name = escapeHtml(firstName) ?? 'Unknown'
				root.username = username ?? 'Unknown'
			}

			root.first_name = escapeHtml(firstName) ?? 'Unknown'
			root.username = username

			session.root = root
			i18n.locale(session.root.settings.lang)
			return next().then(async () => {
				await session.root.save()
			})
		})
		.catch(err => console.log(err.message))
}
